import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from '../employee';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(emps: Array<Employee>, ...args: string[]): unknown {
    let param = args[0]
    // console.log(param)
    console.log(emps)
    let filteredList: Employee[] = emps.filter(emp => {
      emp.name.startsWith(param)
    })
    console.log(filteredList)
    return filteredList
  }

}
