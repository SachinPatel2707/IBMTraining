export class Employee {
    name: string
    id: number
    salary: number

    constructor(id: number, name: string, salary: number) {
        this.name = name
        this.id = id
        this.salary = salary
    }
}
