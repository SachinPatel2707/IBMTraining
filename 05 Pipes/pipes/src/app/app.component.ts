import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from './employee';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pipes';
  data: any = []
  objdata: Employee[] = []

  private _jsonUrl = '../assets/data.json'

  constructor(private http: HttpClient) {

  }

  ngOnInit() {
    this.getData()
  }

  getData() {
    this.http.get(this._jsonUrl).subscribe(data => {
      // this.keys = Object.keys(data)
      this.data = Object.values(data)
      this.data = this.data[0]
      // console.log(this.data)

      for(let i = 0; i < this.data.length; i++) {
        let el = this.data[i]
        this.objdata.push(new Employee(el.id, el.name, el.salary))
      }
    })
  }

}
