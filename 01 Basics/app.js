axios.get("http://localhost:8000/api/search")
.then((response) => {
    response.data.forEach(el => {
        var html = `
        <tr>
            <td>${el.flight_no}</td>
            <td>${el.source}</td>
            <td>${el.destination}</td>
            <td>${el.price}</td>
        </tr>
        `
        document.getElementById("tb").insertAdjacentHTML('beforeend', html)
    });
}, (error) => {
    console.error(error)
})

