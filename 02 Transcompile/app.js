var Greeter = /** @class */ (function () {
    function Greeter(msg) {
        this.greeter = msg;
    }
    Greeter.prototype.greet = function () {
        return "Hello " + this.greeter;
    };
    return Greeter;
}());
var greeter = new Greeter("Sachin");
console.log(greeter.greet());
