class Greeter {
    greeter: String
    
    constructor(msg: String) {
        this.greeter = msg
    }

    greet() {
        return `Hello ${this.greeter}`
    }
}

let greeter = new Greeter("Sachin")
console.log(greeter.greet())