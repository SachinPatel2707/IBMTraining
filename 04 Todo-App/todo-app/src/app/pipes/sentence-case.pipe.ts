import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sentenceCase'
})
export class SentenceCasePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    value.toLowerCase()
    return value.charAt(0).toUpperCase() + value.substr(1, value.length)
  }

}
