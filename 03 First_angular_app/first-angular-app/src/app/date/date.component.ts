import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-date',
  template: `<h2>Today's date is {{ now }}`,
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {

  now: Date 
  constructor() { 
    this.now = new Date()
  }

  ngOnInit(): void {
  }

}
